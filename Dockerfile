FROM python:3.9

RUN set -x \
 && apt update \
 && apt upgrade -y \
 && apt install -y firefox-esr

# Add latest FireFox
RUN set -x \
 && apt install -y libx11-xcb1 libdbus-glib-1-2 \
 && FIREFOX_VER=latest \
 && wget -O firefox-${FIREFOX_VER}.tar.bz2 "https://download.mozilla.org/?product=firefox-${FIREFOX_VER}&os=linux64&lang=en-US" \
 && tar -jxf firefox-* \
 && mv firefox /opt/ \
 && chmod 755 /opt/firefox \
 && chmod 755 /opt/firefox/firefox

# Add geckodriver
RUN set -x \
 && GECKODRIVER_VER=`wget https://github.com/mozilla/geckodriver/releases/latest -q -O - | egrep -o 'v[0-9]+.[0-9]+.[0-9]+' | head -n 1` \
 && curl -sSLO https://github.com/mozilla/geckodriver/releases/download/${GECKODRIVER_VER}/geckodriver-${GECKODRIVER_VER}-linux64.tar.gz \
 && tar zxf geckodriver-*.tar.gz \
 && mv geckodriver /usr/bin/
 
WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./src /app

CMD ["uvicorn", "api:app", "--host", "0.0.0.0", "--port", "8084"]