# TextGrid Python sid API

This package delivers a TextGrid session ID (SID) via GET/POST request.

## Usage

The package makes a login via [https://textgridlab.org/1.0/Shibboleth.sso/Login?target=/1.0/secure/TextGrid-WebAuth.php?authZinstance=textgrid-esx2.gwdg.de](https://textgridlab.org/1.0/Shibboleth.sso/Login?target=/1.0/secure/TextGrid-WebAuth.php?authZinstance=textgrid-esx2.gwdg.de), so you first need login credentials (USERNAME and PASSWORD).

The dev instance is currently available at [sid.eupt-dev.sub.uni-goettingen.de](https://sid.eupt-dev.sub.uni-goettingen.de).

<!--
### Via GET

In your browser, you can get an SID via the following URL:

```
https://sid.eupt-dev.sub.uni-goettingen.de?username=USERNAME&password=PASSWORD
```

### Via POST
-->

In Python, you can get an SID as follows:

```python
sid = requests.post("https://sid.eupt-dev.sub.uni-goettingen.de/", json={"username": USERNAME, "password": PASSWORD}).json()

# or:

sid = requests.post("https://sid.eupt-dev.sub.uni-goettingen.de/", json={"username": USERNAME, "password": PASSWORD}).text.strip('"')
```

## Test

For testing, enter your login credentials in the existing file `.env` or in a new file `.env.local`. Then run the test script:

```
pytest test/test_api.py -s
```

## Contributors

* **Tillmann Dönicke** - *Initial work* - [tillmann.doenicke](https://gitlab.gwdg.de/tillmann.doenicke)

These sources have been very useful for setting up Selenium in Docker:
- [Example of Selenium with Python on Docker with latest FireFox](https://takac.dev/example-of-selenium-with-python-on-docker-with-latest-firefox/)
- [dockselpy: Dockerized Selenium and Python with support for Chrome, Firefox and PhantomJS](https://github.com/dimmg/dockselpy)
