import os

import requests
from dotenv import dotenv_values

# read the credentials from `.env.local` (if existing) or `.env` (otherwise)
dotenv_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", ".env")
if os.path.exists(dotenv_path + ".local"):
    secrets = dotenv_values(dotenv_path=dotenv_path + ".local")
else:
    secrets = dotenv_values(dotenv_path=dotenv_path)


def test_api():
    """Make a test request to the API."""
    data = {"username": secrets["USERNAME"], "password": secrets["PASSWORD"]}
    response = requests.post("https://sid.eupt-dev.sub.uni-goettingen.de/", json=data)
    res = response.json()
    print(res)
    assert isinstance(res, str)


if __name__ == "__main__":
    test_api()
