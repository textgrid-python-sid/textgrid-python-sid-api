import urllib.parse
from typing import Dict

import pandas as pd
from fastapi import FastAPI, HTTPException
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

app = FastAPI()


@app.get("/healthz")
def get_health() -> Dict[str, str]:
    """Endpoint for K8s liveness probe."""
    return {"status": "ok"}


'''
@app.get("/")
async def get_sid(username: str, password: str) -> str:
    """Deliver the sid via GET."""
    username = urllib.parse.unquote(username)
    password = urllib.parse.unquote(password)
    return create_sid(username, password)
'''


@app.post("/")
async def post_sid(data: Dict[str, str]) -> str:
    """Deliver the sid via POST."""
    username = data["username"]
    password = data["password"]
    return create_sid(username, password)


def create_sid(username: str, password: str) -> str:
    """Log in into Dariah and return a new sid.

    Args:
        username: Username.
        password: Password.

    Returns:
        The new sid.

    """
    options = Options()
    options.headless = True  # old
    options.add_argument("--headless")  # new
    service = Service(executable_path="/usr/bin/geckodriver")
    driver = webdriver.Firefox(options=options, service=service)
    driver.get(
        "https://textgridlab.org/1.0/Shibboleth.sso/Login?target=/1.0/secure/TextGrid-WebAuth.php?authZinstance=textgrid-esx2.gwdg.de"
    )
    dariah_button = get_element(driver, By.NAME, "Select2")
    dariah_button.click()
    field_username = get_element(driver, By.NAME, "username")
    field_password = get_element(driver, By.NAME, "password")
    field_username.send_keys(username)
    field_password.send_keys(password)
    login_button = get_element(driver, By.ID, "loginbutton")
    login_button.click()
    table = get_element(driver, By.TAG_NAME, "table")
    table_html = table.get_attribute("outerHTML")
    table_dict = {
        k: v.iloc[0, 1].strip() for k, v in pd.read_html(table_html)[0].groupby(0)
    }
    driver.quit()
    if "TgAuth-Sitzungs-ID" in table_dict:  # German
        return table_dict["TgAuth-Sitzungs-ID"]
    elif "TgAuth Session ID" in table_dict:  # English
        return table_dict["TgAuth Session ID"]
    else:  # any other language
        for key in table_dict:
            if "TgAuth" in key and "ID" in key:
                return table_dict[key]
        raise HTTPException(
            status_code=500,
            detail="Oops! Something went wrong. Perhaps you are communicating in an unknown language.",
        )


def get_element(driver: WebDriver, by: str, value: str) -> WebElement:
    """Returns `driver.find_element(by, value)` as soon as the element is present.
    Raises an `HTTPException` if the element could not be found.
    """
    timeout = 5
    try:
        element_present = EC.presence_of_element_located((by, value))
        WebDriverWait(driver, timeout).until(element_present)
    except TimeoutException as ex:
        driver.quit()
        raise HTTPException(
            status_code=500,
            detail="Oops! Something went wrong. Perhaps your credentials are invalid.",
        ) from ex
    return driver.find_element(by, value)
